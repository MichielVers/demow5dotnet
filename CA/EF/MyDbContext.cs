using CA.Domain;
using Microsoft.EntityFrameworkCore;

namespace CA.EF
{
    public class MyDbContext : DbContext
    {
        public DbSet<Person> People { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
           // base.OnConfiguring(optionsBuilder);
           optionsBuilder.UseSqlite(@"Data Source=mydb.db;");
        }
    }
}