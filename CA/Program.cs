﻿using System;
using CA.Domain;
using CA.EF;
using Microsoft.EntityFrameworkCore;

namespace CA
{
    class Program
    {
        static void Main(string[] args)
        {

            MyDbContext ctx = new MyDbContext();

            DbSet<Person> result = ctx.People;
            
            Console.WriteLine(result);
        }
    }
}